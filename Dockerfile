FROM python:3.12-alpine3.19

# Grab the repositiories so we can install things we will need.
COPY repositories /etc/apk/repositories

# This is the manifest of the packages we need to install.
COPY requirements.txt /tmp/requirements.txt

# Copy the files the system needs to run.
COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY etc /etc

RUN apk update && \
    apk add --no-cache dnsmasq curl openssh-client && \
    pip install --upgrade pip && \
    pip install -r /tmp/requirements.txt && \
    # Enable docker-entrypoint.sh to be executable
    chmod +x /docker-entrypoint.sh && \
    # Install Netbird
    curl -fsSL https://pkgs.netbird.io/install.sh | sh && \
    # Do all of our work before cleaning up.
    rm -rf /tmp/requirements.txt && \
    rm -rf /etc/apk/repositories

ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD [ "python3" ]
