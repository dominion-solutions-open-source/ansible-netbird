#!/usr/bin/env sh
# docker-entrypoint.sh
if [ -z "$NETBIRD_SETUP_KEY" ]; then
  echo "NETBIRD_SETUP_KEY is not set. Please set it to the setup key provided by Netbird."
  exit 1
fi

## Start Netbird
if [ -z "$1" ]; then
  supervisord
  else
    supervisord  &
    exec "$@"
fi
