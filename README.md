Ansible Netbird
---
![Ansible Mark](images/Ansible-Mark-RGB_Black.svg){width=100px height=100px}&nbsp;
![Netbird Mark](images/netbird.png){width=100px height=100px}

This container contains Ansible, DNSMasq and Netbird.


- [Tagging Conventions](#tagging-conventions)
  - [Netbird Version](#netbird-version)
- [Usage](#usage)
- [As a GitLab Builder](#as-a-gitlab-builder)
  - [Create a Setup Key in Netbird](#create-a-setup-key-in-netbird)
  - [Set up your Environment Variables](#set-up-your-environment-variables)
  - [Build your .gitlab-ci.yml](#build-your-gitlab-ciyml)
- [Credits](#credits)

# Tagging Conventions
Tagging follows the format `ansible<ansible-version>-<distribution><version>`.  For example, the tag `ansible2.16-alpine3.19` would denote that the base image is built with `ansible-core 2.16` on `alpine 3.19`.  There is **intentionally no `latest` tag** on the repository.

## Netbird Version
The Netbird version is the Latest Netbird Version at the time of building.  Right now it is `0.26.2`.  This updates each time this container is built.

# Usage
In order to run the project, a few adjustments need to be made.  As this modifies the underlying network, it needs the capability `NET_ADMIN`.  This will allow it to be able to make the connection to the Netbird network.  It also requires the `NETBIRD_SETUP_KEY` environment variable to be set in order to provision itself as a Netbird Peer.
```bash
docker run -it -e NETBIRD_SETUP_KEY "00000000-0000-0000-0000-000000000000" --cap-add=NET_ADMIN registry.gitlab.com/dominion-solutions-open-source/ansible-netbird:ansible2.16-alpine3.19 ash
 ```

# As a GitLab Builder
 This image can be used as a GitLab Builder.  It is a public image and therefore anyone can pull it.

## Create a Setup Key in Netbird
1. Navigate to the Setup Keys Menu in the Netbird App.
2. Select `Create Setup Key`
3. Fill in the Setup Key form, paying special attention to the _Usage Limit_, _Expires in_, and _Ephemeral Peers_ fields.

![Recommended settings set in Netbird with Usage Limit set to Unlimited, Expires in set to 30 days, and Ephemeral Peers set to True](images/netbird_setup.png)

## Set up your Environment Variables
1. Navigate to your project.
2. Select CI/CD on the Right
3. Expand the Variables
4. Create a variable called `NETBIRD_SETUP_KEY` and put your setup key into it.
![Environment variables showing the NETBIRD_SETUP_KEY set in the project](images/ansible_env_variables.png)

## Build your .gitlab-ci.yml
There is one area where special attention is needed when creating the build script:  `FF_NETWORK_PER_BUILD` must be `true`.

Here is an example build
```yaml
stages:
  - deploy
variables:
  FF_NETWORK_PER_BUILD: "true"
run-ansible-deploy:
  stage: deploy
  image:
    name: registry.gitlab.com/dominion-solutions-open-source/ansible-netbird:ansible2.16-alpine3.19
  before_script:
    - pip install -r requirements.txt
    - ansible-galaxy collection install -r requirements.yml
  variables:
    # Required variables here
  script: |
    ansible-inventory -i inventories/test --graph
    ansible-playbook -i inventory/test playbooks/ansible-deploy.yml
  environment:
    name: test
```

# Credits
- Mark J. Horninger (@spam-n-eggs)
- [All Contributors](https://gitlab.com/dominion-solutions-open-source/ansible-netbird/-/graphs/ansible2.16-alpine3.19?ref_type=heads)
